﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSB_CR
{
    public partial class PraticienOuVisiteur : Form
    {
        public PraticienOuVisiteur()
        {
            InitializeComponent();
        }

        private void btnPraticien_Click(object sender, EventArgs e)
        {
            AuthentificationPraticien form1 = new AuthentificationPraticien();
            form1.FormClosing += new FormClosingEventHandler(form5_FormClosing);
            this.Hide();
            form1.Show();
        }

        private void form5_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Close();
        }

        private void btnVisiteur_Click(object sender, EventArgs e)
        {
            AuthentificationVisiteur form6 = new AuthentificationVisiteur();
            form6.FormClosing += new FormClosingEventHandler(form5_FormClosing);
            this.Hide();
            form6.Show();
        }
    }
}

﻿namespace GSB_CR
{
    partial class GestionPraticien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GestionPraticien));
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.cbxAjoutTypePraticien = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCodePostal = new System.Windows.Forms.TextBox();
            this.txtVille = new System.Windows.Forms.TextBox();
            this.txtAdresse = new System.Windows.Forms.TextBox();
            this.txtPrenom = new System.Windows.Forms.TextBox();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.cbxNomPrenomModifPraticien = new System.Windows.Forms.ComboBox();
            this.btnModifier = new System.Windows.Forms.Button();
            this.cbxModifTypePraticien = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCPModifPraticien = new System.Windows.Forms.TextBox();
            this.txtVilleModifPraticien = new System.Windows.Forms.TextBox();
            this.txtAdresseModifPraticien = new System.Windows.Forms.TextBox();
            this.txtPrenomModifPraticien = new System.Windows.Forms.TextBox();
            this.txtNomModifPraticien = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.cbxSupprPraticien = new System.Windows.Forms.ComboBox();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(787, 425);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Liste des praticiens";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column6,
            this.Column5});
            this.dataGridView1.Location = new System.Drawing.Point(6, 6);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(775, 413);
            this.dataGridView1.TabIndex = 0;
            // 
            // Column1
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column1.HeaderText = "Numero praticien";
            this.Column1.Name = "Column1";
            this.Column1.Width = 103;
            // 
            // Column2
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column2.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column2.HeaderText = "Nom";
            this.Column2.Name = "Column2";
            this.Column2.Width = 54;
            // 
            // Column3
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column3.HeaderText = "Prenom";
            this.Column3.Name = "Column3";
            this.Column3.Width = 68;
            // 
            // Column4
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column4.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column4.HeaderText = "Adresse";
            this.Column4.Name = "Column4";
            this.Column4.Width = 70;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Ville";
            this.Column6.Name = "Column6";
            this.Column6.Width = 51;
            // 
            // Column5
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column5.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column5.HeaderText = "Type praticien";
            this.Column5.Name = "Column5";
            this.Column5.Width = 91;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 59);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(795, 451);
            this.tabControl1.TabIndex = 20;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(209)))), ((int)(((byte)(234)))));
            this.tabPage1.Controls.Add(this.btnEnregistrer);
            this.tabPage1.Controls.Add(this.cbxAjoutTypePraticien);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.txtCodePostal);
            this.tabPage1.Controls.Add(this.txtVille);
            this.tabPage1.Controls.Add(this.txtAdresse);
            this.tabPage1.Controls.Add(this.txtPrenom);
            this.tabPage1.Controls.Add(this.txtNom);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(787, 425);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Ajouter un praticien";
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.Location = new System.Drawing.Point(331, 391);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(108, 23);
            this.btnEnregistrer.TabIndex = 61;
            this.btnEnregistrer.Text = "Enregistrer";
            this.btnEnregistrer.UseVisualStyleBackColor = true;
            this.btnEnregistrer.Click += new System.EventHandler(this.btnEnregistrer_Click);
            // 
            // cbxAjoutTypePraticien
            // 
            this.cbxAjoutTypePraticien.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxAjoutTypePraticien.FormattingEnabled = true;
            this.cbxAjoutTypePraticien.Location = new System.Drawing.Point(325, 364);
            this.cbxAjoutTypePraticien.Name = "cbxAjoutTypePraticien";
            this.cbxAjoutTypePraticien.Size = new System.Drawing.Size(121, 21);
            this.cbxAjoutTypePraticien.TabIndex = 60;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label7.Location = new System.Drawing.Point(317, 340);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(141, 23);
            this.label7.TabIndex = 59;
            this.label7.Text = "Type de praticien :";
            // 
            // txtCodePostal
            // 
            this.txtCodePostal.Location = new System.Drawing.Point(242, 317);
            this.txtCodePostal.Name = "txtCodePostal";
            this.txtCodePostal.Size = new System.Drawing.Size(108, 20);
            this.txtCodePostal.TabIndex = 58;
            // 
            // txtVille
            // 
            this.txtVille.Location = new System.Drawing.Point(419, 317);
            this.txtVille.Name = "txtVille";
            this.txtVille.Size = new System.Drawing.Size(108, 20);
            this.txtVille.TabIndex = 56;
            // 
            // txtAdresse
            // 
            this.txtAdresse.Location = new System.Drawing.Point(242, 268);
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.Size = new System.Drawing.Size(285, 20);
            this.txtAdresse.TabIndex = 51;
            // 
            // txtPrenom
            // 
            this.txtPrenom.Location = new System.Drawing.Point(331, 219);
            this.txtPrenom.Name = "txtPrenom";
            this.txtPrenom.Size = new System.Drawing.Size(108, 20);
            this.txtPrenom.TabIndex = 50;
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(331, 170);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(108, 20);
            this.txtNom.TabIndex = 49;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.Location = new System.Drawing.Point(246, 291);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 23);
            this.label6.TabIndex = 57;
            this.label6.Text = "Code Postal :";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.Location = new System.Drawing.Point(451, 291);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 23);
            this.label5.TabIndex = 55;
            this.label5.Text = "Ville :";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.Location = new System.Drawing.Point(350, 242);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 23);
            this.label4.TabIndex = 54;
            this.label4.Text = "Adresse :";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(350, 193);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 23);
            this.label3.TabIndex = 53;
            this.label3.Text = "Prenom :";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(361, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 23);
            this.label2.TabIndex = 52;
            this.label2.Text = "Nom :";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(209)))), ((int)(((byte)(234)))));
            this.tabPage3.Controls.Add(this.cbxNomPrenomModifPraticien);
            this.tabPage3.Controls.Add(this.btnModifier);
            this.tabPage3.Controls.Add(this.cbxModifTypePraticien);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.txtCPModifPraticien);
            this.tabPage3.Controls.Add(this.txtVilleModifPraticien);
            this.tabPage3.Controls.Add(this.txtAdresseModifPraticien);
            this.tabPage3.Controls.Add(this.txtPrenomModifPraticien);
            this.tabPage3.Controls.Add(this.txtNomModifPraticien);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.pictureBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(787, 425);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Modification d\'un praticien";
            // 
            // cbxNomPrenomModifPraticien
            // 
            this.cbxNomPrenomModifPraticien.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxNomPrenomModifPraticien.FormattingEnabled = true;
            this.cbxNomPrenomModifPraticien.Location = new System.Drawing.Point(317, 112);
            this.cbxNomPrenomModifPraticien.Name = "cbxNomPrenomModifPraticien";
            this.cbxNomPrenomModifPraticien.Size = new System.Drawing.Size(137, 21);
            this.cbxNomPrenomModifPraticien.TabIndex = 62;
            this.cbxNomPrenomModifPraticien.SelectedIndexChanged += new System.EventHandler(this.cbxModifPraticien_SelectedIndexChanged);
            // 
            // btnModifier
            // 
            this.btnModifier.Location = new System.Drawing.Point(331, 391);
            this.btnModifier.Name = "btnModifier";
            this.btnModifier.Size = new System.Drawing.Size(108, 23);
            this.btnModifier.TabIndex = 61;
            this.btnModifier.Text = "Modifier";
            this.btnModifier.UseVisualStyleBackColor = true;
            this.btnModifier.Click += new System.EventHandler(this.btnModifier_Click);
            // 
            // cbxModifTypePraticien
            // 
            this.cbxModifTypePraticien.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxModifTypePraticien.Enabled = false;
            this.cbxModifTypePraticien.FormattingEnabled = true;
            this.cbxModifTypePraticien.Location = new System.Drawing.Point(325, 364);
            this.cbxModifTypePraticien.Name = "cbxModifTypePraticien";
            this.cbxModifTypePraticien.Size = new System.Drawing.Size(121, 21);
            this.cbxModifTypePraticien.TabIndex = 60;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label8.Location = new System.Drawing.Point(317, 340);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(141, 23);
            this.label8.TabIndex = 59;
            this.label8.Text = "Type de praticien :";
            // 
            // txtCPModifPraticien
            // 
            this.txtCPModifPraticien.Enabled = false;
            this.txtCPModifPraticien.Location = new System.Drawing.Point(242, 317);
            this.txtCPModifPraticien.Name = "txtCPModifPraticien";
            this.txtCPModifPraticien.Size = new System.Drawing.Size(108, 20);
            this.txtCPModifPraticien.TabIndex = 58;
            // 
            // txtVilleModifPraticien
            // 
            this.txtVilleModifPraticien.Enabled = false;
            this.txtVilleModifPraticien.Location = new System.Drawing.Point(419, 317);
            this.txtVilleModifPraticien.Name = "txtVilleModifPraticien";
            this.txtVilleModifPraticien.Size = new System.Drawing.Size(108, 20);
            this.txtVilleModifPraticien.TabIndex = 56;
            // 
            // txtAdresseModifPraticien
            // 
            this.txtAdresseModifPraticien.Enabled = false;
            this.txtAdresseModifPraticien.Location = new System.Drawing.Point(242, 268);
            this.txtAdresseModifPraticien.Name = "txtAdresseModifPraticien";
            this.txtAdresseModifPraticien.Size = new System.Drawing.Size(285, 20);
            this.txtAdresseModifPraticien.TabIndex = 51;
            // 
            // txtPrenomModifPraticien
            // 
            this.txtPrenomModifPraticien.Enabled = false;
            this.txtPrenomModifPraticien.Location = new System.Drawing.Point(331, 219);
            this.txtPrenomModifPraticien.Name = "txtPrenomModifPraticien";
            this.txtPrenomModifPraticien.Size = new System.Drawing.Size(108, 20);
            this.txtPrenomModifPraticien.TabIndex = 50;
            // 
            // txtNomModifPraticien
            // 
            this.txtNomModifPraticien.Enabled = false;
            this.txtNomModifPraticien.Location = new System.Drawing.Point(331, 170);
            this.txtNomModifPraticien.Name = "txtNomModifPraticien";
            this.txtNomModifPraticien.Size = new System.Drawing.Size(108, 20);
            this.txtNomModifPraticien.TabIndex = 49;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label9.Location = new System.Drawing.Point(246, 291);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 23);
            this.label9.TabIndex = 57;
            this.label9.Text = "Code Postal :";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label10.Location = new System.Drawing.Point(451, 291);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 23);
            this.label10.TabIndex = 55;
            this.label10.Text = "Ville :";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label11.Location = new System.Drawing.Point(350, 242);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 23);
            this.label11.TabIndex = 54;
            this.label11.Text = "Adresse :";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label12.Location = new System.Drawing.Point(350, 193);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 23);
            this.label12.TabIndex = 53;
            this.label12.Text = "Prenom :";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label13.Location = new System.Drawing.Point(361, 144);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 23);
            this.label13.TabIndex = 52;
            this.label13.Text = "Nom :";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(209)))), ((int)(((byte)(234)))));
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.cbxSupprPraticien);
            this.tabPage4.Controls.Add(this.btnSupprimer);
            this.tabPage4.Controls.Add(this.pictureBox3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(787, 425);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "Supprimer un praticien";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label14.Location = new System.Drawing.Point(329, 283);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(110, 23);
            this.label14.TabIndex = 64;
            this.label14.Text = "Prenom - Nom";
            // 
            // cbxSupprPraticien
            // 
            this.cbxSupprPraticien.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSupprPraticien.FormattingEnabled = true;
            this.cbxSupprPraticien.Location = new System.Drawing.Point(317, 309);
            this.cbxSupprPraticien.Name = "cbxSupprPraticien";
            this.cbxSupprPraticien.Size = new System.Drawing.Size(137, 21);
            this.cbxSupprPraticien.TabIndex = 63;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.Location = new System.Drawing.Point(331, 391);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(108, 23);
            this.btnSupprimer.TabIndex = 61;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = true;
            this.btnSupprimer.Click += new System.EventHandler(this.btnSupprimer_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.label1.Location = new System.Drawing.Point(198, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(417, 69);
            this.label1.TabIndex = 5;
            this.label1.Text = "Gestion des praticiens";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::GSB_CR.Properties.Resources.praticien;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(333, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(108, 122);
            this.pictureBox1.TabIndex = 48;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::GSB_CR.Properties.Resources.praticien;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(340, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(93, 100);
            this.pictureBox2.TabIndex = 48;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::GSB_CR.Properties.Resources.praticien;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(297, 60);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(178, 193);
            this.pictureBox3.TabIndex = 48;
            this.pictureBox3.TabStop = false;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(209)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(822, 522);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestion des praticiens";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form4_FormClosed);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.ComboBox cbxAjoutTypePraticien;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCodePostal;
        private System.Windows.Forms.TextBox txtVille;
        private System.Windows.Forms.TextBox txtAdresse;
        private System.Windows.Forms.TextBox txtPrenom;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox cbxNomPrenomModifPraticien;
        private System.Windows.Forms.Button btnModifier;
        private System.Windows.Forms.ComboBox cbxModifTypePraticien;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCPModifPraticien;
        private System.Windows.Forms.TextBox txtVilleModifPraticien;
        private System.Windows.Forms.TextBox txtAdresseModifPraticien;
        private System.Windows.Forms.TextBox txtPrenomModifPraticien;
        private System.Windows.Forms.TextBox txtNomModifPraticien;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ComboBox cbxSupprPraticien;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label14;
    }
}
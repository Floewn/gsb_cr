﻿namespace GSB_CR
{
    partial class GestionCR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GestionCR));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cBx_numRapp = new System.Windows.Forms.ComboBox();
            this.btn_rechercher = new System.Windows.Forms.Button();
            this.cBx_nom2 = new System.Windows.Forms.ComboBox();
            this.txt_motif2 = new System.Windows.Forms.TextBox();
            this.txt_Bilan2 = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTime2 = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btn_creer = new System.Windows.Forms.Button();
            this.cBx_nom1 = new System.Windows.Forms.ComboBox();
            this.txt_motif1 = new System.Windows.Forms.TextBox();
            this.txt_Bilan1 = new System.Windows.Forms.RichTextBox();
            this.txt_numRap = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTime1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.GestionCompteRendus = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.cBx_numRapp2 = new System.Windows.Forms.ComboBox();
            this.btn_modifier = new System.Windows.Forms.Button();
            this.cbx_Praticien = new System.Windows.Forms.ComboBox();
            this.txt_motif = new System.Windows.Forms.TextBox();
            this.txt_bilan = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.cbx_ecoute = new System.Windows.Forms.ComboBox();
            this.cbx_interet = new System.Windows.Forms.ComboBox();
            this.cbx_niveau = new System.Windows.Forms.ComboBox();
            this.cbx_niveau2 = new System.Windows.Forms.ComboBox();
            this.cbx_interet2 = new System.Windows.Forms.ComboBox();
            this.cbx_ecoute2 = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(469, 456);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.cbx_niveau);
            this.tabPage2.Controls.Add(this.cbx_interet);
            this.tabPage2.Controls.Add(this.cbx_ecoute);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.cBx_numRapp);
            this.tabPage2.Controls.Add(this.btn_rechercher);
            this.tabPage2.Controls.Add(this.cBx_nom2);
            this.tabPage2.Controls.Add(this.txt_motif2);
            this.tabPage2.Controls.Add(this.txt_Bilan2);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.dateTime2);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(461, 430);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Consulter un compte rendu";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // cBx_numRapp
            // 
            this.cBx_numRapp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBx_numRapp.FormattingEnabled = true;
            this.cBx_numRapp.Location = new System.Drawing.Point(110, 95);
            this.cBx_numRapp.Name = "cBx_numRapp";
            this.cBx_numRapp.Size = new System.Drawing.Size(100, 21);
            this.cBx_numRapp.TabIndex = 27;
            // 
            // btn_rechercher
            // 
            this.btn_rechercher.Location = new System.Drawing.Point(166, 401);
            this.btn_rechercher.Name = "btn_rechercher";
            this.btn_rechercher.Size = new System.Drawing.Size(137, 23);
            this.btn_rechercher.TabIndex = 3;
            this.btn_rechercher.Text = "Rechercher";
            this.btn_rechercher.UseVisualStyleBackColor = true;
            this.btn_rechercher.Click += new System.EventHandler(this.btn_rechercher_Click);
            // 
            // cBx_nom2
            // 
            this.cBx_nom2.Enabled = false;
            this.cBx_nom2.FormattingEnabled = true;
            this.cBx_nom2.Location = new System.Drawing.Point(296, 95);
            this.cBx_nom2.Name = "cBx_nom2";
            this.cBx_nom2.Size = new System.Drawing.Size(140, 21);
            this.cBx_nom2.TabIndex = 26;
            // 
            // txt_motif2
            // 
            this.txt_motif2.Enabled = false;
            this.txt_motif2.Location = new System.Drawing.Point(296, 124);
            this.txt_motif2.Name = "txt_motif2";
            this.txt_motif2.Size = new System.Drawing.Size(140, 20);
            this.txt_motif2.TabIndex = 25;
            // 
            // txt_Bilan2
            // 
            this.txt_Bilan2.Enabled = false;
            this.txt_Bilan2.Location = new System.Drawing.Point(12, 284);
            this.txt_Bilan2.Name = "txt_Bilan2";
            this.txt_Bilan2.Size = new System.Drawing.Size(444, 110);
            this.txt_Bilan2.TabIndex = 24;
            this.txt_Bilan2.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 259);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Bilan :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(254, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Motif :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(236, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Praticien :";
            // 
            // dateTime2
            // 
            this.dateTime2.CustomFormat = "dd/mm/yyyy";
            this.dateTime2.Enabled = false;
            this.dateTime2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTime2.Location = new System.Drawing.Point(110, 124);
            this.dateTime2.Name = "dateTime2";
            this.dateTime2.Size = new System.Drawing.Size(100, 20);
            this.dateTime2.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(68, 126);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Date :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 98);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "N° de Rapport :";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(450, 72);
            this.label6.TabIndex = 16;
            this.label6.Text = "Gestion des Comptes Rendus";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cbx_niveau2);
            this.tabPage1.Controls.Add(this.cbx_interet2);
            this.tabPage1.Controls.Add(this.cbx_ecoute2);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.btn_creer);
            this.tabPage1.Controls.Add(this.cBx_nom1);
            this.tabPage1.Controls.Add(this.txt_motif1);
            this.tabPage1.Controls.Add(this.txt_Bilan1);
            this.tabPage1.Controls.Add(this.txt_numRap);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.dateTime1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.GestionCompteRendus);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(461, 430);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Rédaction d\'un compte rendu";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btn_creer
            // 
            this.btn_creer.Location = new System.Drawing.Point(166, 401);
            this.btn_creer.Name = "btn_creer";
            this.btn_creer.Size = new System.Drawing.Size(137, 23);
            this.btn_creer.TabIndex = 14;
            this.btn_creer.Text = "Créer";
            this.btn_creer.UseVisualStyleBackColor = true;
            this.btn_creer.Click += new System.EventHandler(this.btn_creer_Click);
            // 
            // cBx_nom1
            // 
            this.cBx_nom1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBx_nom1.FormattingEnabled = true;
            this.cBx_nom1.Location = new System.Drawing.Point(296, 95);
            this.cBx_nom1.Name = "cBx_nom1";
            this.cBx_nom1.Size = new System.Drawing.Size(140, 21);
            this.cBx_nom1.TabIndex = 13;
            // 
            // txt_motif1
            // 
            this.txt_motif1.Location = new System.Drawing.Point(296, 124);
            this.txt_motif1.Name = "txt_motif1";
            this.txt_motif1.Size = new System.Drawing.Size(140, 20);
            this.txt_motif1.TabIndex = 11;
            // 
            // txt_Bilan1
            // 
            this.txt_Bilan1.Location = new System.Drawing.Point(12, 291);
            this.txt_Bilan1.Name = "txt_Bilan1";
            this.txt_Bilan1.Size = new System.Drawing.Size(444, 110);
            this.txt_Bilan1.TabIndex = 10;
            this.txt_Bilan1.Text = "";
            // 
            // txt_numRap
            // 
            this.txt_numRap.Enabled = false;
            this.txt_numRap.Location = new System.Drawing.Point(110, 95);
            this.txt_numRap.Name = "txt_numRap";
            this.txt_numRap.Size = new System.Drawing.Size(100, 20);
            this.txt_numRap.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 275);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Bilan :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(254, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Motif :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(236, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Praticien :";
            // 
            // dateTime1
            // 
            this.dateTime1.CustomFormat = "dd/mm/yyyy";
            this.dateTime1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTime1.Location = new System.Drawing.Point(110, 124);
            this.dateTime1.Name = "dateTime1";
            this.dateTime1.Size = new System.Drawing.Size(100, 20);
            this.dateTime1.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Date :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "N° de Rapport :";
            // 
            // GestionCompteRendus
            // 
            this.GestionCompteRendus.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.GestionCompteRendus.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GestionCompteRendus.Location = new System.Drawing.Point(6, 3);
            this.GestionCompteRendus.Name = "GestionCompteRendus";
            this.GestionCompteRendus.Size = new System.Drawing.Size(450, 72);
            this.GestionCompteRendus.TabIndex = 0;
            this.GestionCompteRendus.Text = "Gestion des Comptes Rendus";
            this.GestionCompteRendus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button1);
            this.tabPage4.Controls.Add(this.cBx_numRapp2);
            this.tabPage4.Controls.Add(this.btn_modifier);
            this.tabPage4.Controls.Add(this.cbx_Praticien);
            this.tabPage4.Controls.Add(this.txt_motif);
            this.tabPage4.Controls.Add(this.txt_bilan);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.dateTimePicker1);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(461, 430);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "Editer un compte rendu";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(71, 400);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(137, 23);
            this.button1.TabIndex = 28;
            this.button1.Text = "Rechercher";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cBx_numRapp2
            // 
            this.cBx_numRapp2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBx_numRapp2.FormattingEnabled = true;
            this.cBx_numRapp2.Location = new System.Drawing.Point(110, 95);
            this.cBx_numRapp2.Name = "cBx_numRapp2";
            this.cBx_numRapp2.Size = new System.Drawing.Size(100, 21);
            this.cBx_numRapp2.TabIndex = 27;
            // 
            // btn_modifier
            // 
            this.btn_modifier.Location = new System.Drawing.Point(257, 401);
            this.btn_modifier.Name = "btn_modifier";
            this.btn_modifier.Size = new System.Drawing.Size(137, 23);
            this.btn_modifier.TabIndex = 3;
            this.btn_modifier.Text = "Modifier";
            this.btn_modifier.UseVisualStyleBackColor = true;
            this.btn_modifier.Click += new System.EventHandler(this.btn_modifier_Click);
            // 
            // cbx_Praticien
            // 
            this.cbx_Praticien.FormattingEnabled = true;
            this.cbx_Praticien.Location = new System.Drawing.Point(296, 95);
            this.cbx_Praticien.Name = "cbx_Praticien";
            this.cbx_Praticien.Size = new System.Drawing.Size(140, 21);
            this.cbx_Praticien.TabIndex = 26;
            // 
            // txt_motif
            // 
            this.txt_motif.Location = new System.Drawing.Point(296, 124);
            this.txt_motif.Name = "txt_motif";
            this.txt_motif.Size = new System.Drawing.Size(140, 20);
            this.txt_motif.TabIndex = 25;
            // 
            // txt_bilan
            // 
            this.txt_bilan.Location = new System.Drawing.Point(12, 175);
            this.txt_bilan.Name = "txt_bilan";
            this.txt_bilan.Size = new System.Drawing.Size(444, 219);
            this.txt_bilan.TabIndex = 24;
            this.txt_bilan.Text = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 159);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Bilan :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(254, 127);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "Motif :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(236, 98);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "Praticien :";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/mm/yyyy";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(110, 124);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(100, 20);
            this.dateTimePicker1.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(68, 126);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 13);
            this.label15.TabIndex = 19;
            this.label15.Text = "Date :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(23, 98);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 13);
            this.label16.TabIndex = 17;
            this.label16.Text = "N° de Rapport :";
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 3);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(450, 72);
            this.label17.TabIndex = 16;
            this.label17.Text = "Gestion des Comptes Rendus";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(186, 163);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(90, 13);
            this.label18.TabIndex = 28;
            this.label18.Text = "Qualité d\'écoute :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(173, 203);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(130, 13);
            this.label19.TabIndex = 29;
            this.label19.Text = "Intêret porté aux produits :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(178, 243);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(119, 13);
            this.label20.TabIndex = 30;
            this.label20.Text = "Niveau de prescription :";
            // 
            // cbx_ecoute
            // 
            this.cbx_ecoute.Enabled = false;
            this.cbx_ecoute.FormattingEnabled = true;
            this.cbx_ecoute.Location = new System.Drawing.Point(176, 179);
            this.cbx_ecoute.Name = "cbx_ecoute";
            this.cbx_ecoute.Size = new System.Drawing.Size(121, 21);
            this.cbx_ecoute.TabIndex = 31;
            // 
            // cbx_interet
            // 
            this.cbx_interet.Enabled = false;
            this.cbx_interet.FormattingEnabled = true;
            this.cbx_interet.Location = new System.Drawing.Point(176, 219);
            this.cbx_interet.Name = "cbx_interet";
            this.cbx_interet.Size = new System.Drawing.Size(121, 21);
            this.cbx_interet.TabIndex = 32;
            // 
            // cbx_niveau
            // 
            this.cbx_niveau.Enabled = false;
            this.cbx_niveau.FormattingEnabled = true;
            this.cbx_niveau.Location = new System.Drawing.Point(176, 259);
            this.cbx_niveau.Name = "cbx_niveau";
            this.cbx_niveau.Size = new System.Drawing.Size(121, 21);
            this.cbx_niveau.TabIndex = 33;
            // 
            // cbx_niveau2
            // 
            this.cbx_niveau2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbx_niveau2.FormattingEnabled = true;
            this.cbx_niveau2.Location = new System.Drawing.Point(168, 253);
            this.cbx_niveau2.Name = "cbx_niveau2";
            this.cbx_niveau2.Size = new System.Drawing.Size(121, 21);
            this.cbx_niveau2.TabIndex = 39;
            // 
            // cbx_interet2
            // 
            this.cbx_interet2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbx_interet2.FormattingEnabled = true;
            this.cbx_interet2.Location = new System.Drawing.Point(168, 213);
            this.cbx_interet2.Name = "cbx_interet2";
            this.cbx_interet2.Size = new System.Drawing.Size(121, 21);
            this.cbx_interet2.TabIndex = 38;
            // 
            // cbx_ecoute2
            // 
            this.cbx_ecoute2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbx_ecoute2.FormattingEnabled = true;
            this.cbx_ecoute2.Location = new System.Drawing.Point(168, 173);
            this.cbx_ecoute2.Name = "cbx_ecoute2";
            this.cbx_ecoute2.Size = new System.Drawing.Size(121, 21);
            this.cbx_ecoute2.TabIndex = 37;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(170, 237);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(119, 13);
            this.label21.TabIndex = 36;
            this.label21.Text = "Niveau de prescription :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(165, 197);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(130, 13);
            this.label22.TabIndex = 35;
            this.label22.Text = "Intêret porté aux produits :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(178, 157);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(90, 13);
            this.label23.TabIndex = 34;
            this.label23.Text = "Qualité d\'écoute :";
            // 
            // GestionCR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(209)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(493, 480);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GestionCR";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestion des Comptes Rendus";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox cBx_numRapp;
        private System.Windows.Forms.Button btn_rechercher;
        private System.Windows.Forms.ComboBox cBx_nom2;
        private System.Windows.Forms.TextBox txt_motif2;
        private System.Windows.Forms.RichTextBox txt_Bilan2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTime2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btn_creer;
        private System.Windows.Forms.ComboBox cBx_nom1;
        private System.Windows.Forms.TextBox txt_motif1;
        private System.Windows.Forms.RichTextBox txt_Bilan1;
        private System.Windows.Forms.TextBox txt_numRap;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTime1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label GestionCompteRendus;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ComboBox cBx_numRapp2;
        private System.Windows.Forms.Button btn_modifier;
        private System.Windows.Forms.ComboBox cbx_Praticien;
        private System.Windows.Forms.TextBox txt_motif;
        private System.Windows.Forms.RichTextBox txt_bilan;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cbx_niveau;
        private System.Windows.Forms.ComboBox cbx_interet;
        private System.Windows.Forms.ComboBox cbx_ecoute;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cbx_niveau2;
        private System.Windows.Forms.ComboBox cbx_interet2;
        private System.Windows.Forms.ComboBox cbx_ecoute2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace GSB_CR
{
    public partial class AuthentificationPraticien : Form
    {
        private MySqlConnection connect;

        public AuthentificationPraticien()
        {
            InitializeComponent();
        }
        

        private void Form1_Load(object sender, EventArgs e)
        {
            connect = new MySqlConnection("SERVER=localhost;PORT=3306;DATABASE=gsb_cr;UID=gsbcr;PWD=serge;SslMode=none");
            
        }

        private void buttonConnexion_Click(object sender, EventArgs e)
        {
            try
            {
                connect.Open();
                MySqlCommand count = new MySqlCommand("SELECT COUNT(*) as nbUser FROM session WHERE SES_UTILISATEUR = @user AND SES_PASSWORD = @password", connect);

                count.Parameters.AddWithValue("@user", textBoxIdentifiant.Text);
                count.Parameters.AddWithValue("@password", textBoxMotDePasse.Text);

                count.ExecuteNonQuery();
                int i = int.Parse(count.ExecuteScalar().ToString());

                count.Parameters.Clear();
                connect.Close();

                if (i > 0)
                {
                    textBoxIdentifiant.Text = this.textBoxIdentifiant.Text;
                    textBoxMotDePasse.Text = this.textBoxMotDePasse.Text;
                    Accueil form2 = new Accueil(textBoxIdentifiant.Text);
                    form2.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Utilisateur ou Mot de passe incorrect");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Impossible de se connecter à la base de donnée");
            }
        }
    }
}

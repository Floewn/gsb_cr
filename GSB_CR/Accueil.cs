﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace GSB_CR
{
    public partial class Accueil : Form
    {
        private MySqlConnection connect;
        string user;
        
        public Accueil(String nomPrenom)
        {
            InitializeComponent();
            user = nomPrenom;
        }

        public void Form2_Load(object sender, EventArgs e)
        {
            connect = new MySqlConnection("SERVER=localhost;PORT=3306;DATABASE=gsb_cr;UID=gsbcr;PWD=serge;SslMode=none");
            label3.Text = "Bienvenue à : ";
            connect.Open();
            MySqlDataReader reader;
            MySqlCommand req = new MySqlCommand("SELECT praticien.prenom, praticien.nom FROM praticien, session WHERE praticien.id = session.id_Praticien AND SES_UTILISATEUR LIKE @user ;", connect);
            req.Parameters.AddWithValue("@user", user);
            reader = req.ExecuteReader();
            while (reader.Read())
            {
                label3.Text += reader.GetString("prenom") + " " + reader.GetString("nom");
            }
            req.Parameters.Clear();
            connect.Close();
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

        private void buttonCR_Click(object sender, EventArgs e)
        {
            GestionCR form3 = new GestionCR();
            form3.Show();
        }

        private void buttonPraticien_Click(object sender, EventArgs e)
        {
            GestionPraticien form4 = new GestionPraticien();
            form4.ShowDialog();
        }

        private void buttonVisiteur_Click(object sender, EventArgs e)
        {
            NotePraticien formNote = new NotePraticien();
            formNote.ShowDialog();
        }
    }
}

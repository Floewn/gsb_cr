﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace GSB_CR
{
    public partial class NotePraticien : Form
    {
        private MySqlConnection connect;

        public NotePraticien()
        {
            InitializeComponent();
            connect = new MySqlConnection("SERVER=localhost;PORT=3306;DATABASE=gsb_cr;UID=gsbcr;PWD=serge;SslMode=none");
        }

        public void RemplissageListePraticien()
        {
            connect.Open();
            MySqlDataReader readerListeMoyenne;
            string nomPraticen;
            string prenomPraticien;
            string moyennePraticien;
            MySqlCommand listeMoyenne = new MySqlCommand("SELECT praticien.nom, praticien.prenom, ((AVG(rapport.ecoute) + AVG(rapport.interet) + AVG(rapport.niveau))/3) AS moyenne FROM rapport, praticien WHERE rapport.id_Praticien = praticien.id GROUP BY nom, prenom ORDER BY moyenne ASC LIMIT 3", connect);
            readerListeMoyenne = listeMoyenne.ExecuteReader();
            while (readerListeMoyenne.Read())
            {

                nomPraticen = readerListeMoyenne.GetString("nom");
                prenomPraticien = readerListeMoyenne.GetString("prenom");
                moyennePraticien = readerListeMoyenne.GetString("moyenne");

                dataGridMoyenne.Rows.Add(nomPraticen, prenomPraticien, moyennePraticien);
            }
            listeMoyenne.Parameters.Clear();
            readerListeMoyenne.Close();
        }

        private void NotePraticien_Load(object sender, EventArgs e)
        {
            RemplissageListePraticien();
        }
    }

    

    
}
